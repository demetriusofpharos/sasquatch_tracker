## Pregame:  
To get everything up and running, I spent some time upgrading my old Dockerfile and docker-compose.yml from v2 to v3; the PHP version in the container from 7.1 to 7.r, and my old Phalcon code from v3.2 to v4.0. Planned on using my old Watchdog logger (the name's from Drupal but the code is all mine) but I had an issue getting it going - something with Phalcon v4.0 that messed with Model::save(). Once the scaffold was up and running:  


## Development
(Note: I did not work straight through; I took breaks and worked on a couple other things. This is over the span of about 3 days.)  
1.5H  - basic CRUD API with corresponding UI   
1.0H  - distance calculator   
1.25H - search by tags  
1.0H  - search radius  


## Postgame:
Added API notes to README. Made sure the containers added the data and ran composer so that everything was up and running without any additional work (other than adding an entry to /etc/hosts)
