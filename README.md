# Jolt Assessment

Based on [Phalcon][:phalcon:] plus some of my old code (updated for current PHP and Phalcon versions).  

URL used during development was `sasquatch.docker` - it should work with any URL so long as you change the `internal->api_url` in the YAML config, located here: `core/app/config/yaml/stun.yml`. No other config changes should be necessary. (Other than the corresponding entry in your hosts file, of course.)  


## Schema
I removed the "geometry" field, as it seemed to double the "latitude" and "longitude" fields. With more time, I would have integrated something like [this geocode address snippet][:geocodeAddress:]. That might even improve speed on the distance calculation and nearby results page.  


## Technology
I used Phalcon PHP. In my opinion, it's the best framework out there. It's arguably faster (owing to the core being in Zephyr); offers relatively quick-and-easy development; doesn't need a ton of special knowledge - you can mostly just use raw PHP once the scaffold is running, with the exception of making sure the MVC classes extend from Phalcon to get all the goodies.  


## Challenges
I've never really used Latitude and Longitude for anything. Finding reliable calculations was a little tricky, however, utilizing them proved relatively simple. I wasn't family with `geometry` in mysql, so for this assessment I dropped it rather than spending a lot of time researching it. Obviously in a project environment, I'd research it fully or discuss the benefits/drawbacks with the architect (assuming that wasn't me) and the database admin (again, assuming I wasn't responsible for maintaining the database).  

There werea couple hiccups early on in getting my old docker files and Phalcon scaffold working in new versions, but really, that's more due to the fact that Phalcon changed a few things (such as revamping Model::save and changing how Model::beforeSave/afterSave works) than any project challenge. Ditto my attempts to include my old docker::watchdog clone to make catching errors easier.  

Finally, improvements:
  * It desperately needs a user authentication for both the front and backend. I've done it before, but it wasn't a requirement so I left it out.
  * Add my old Watchdog class back in. It's proved invaluable in the past in tracking down errors that don't necessarily show up in a trace.
  * The success/error/warn messages don't alway show on the front end. Seems Phalcon changed that too, but I didn't spend a ton of time on the front end.
  * Though some results are paginated, there isn't any real way to search or order the results
  * The items in (5) in the original README. Though the first three seem to repeat the items in (3), and are thus basically functional, I don't think it would have been much to finish it off. However, I ran up against the hard time limit.


## API URLS
- Response codes:  
  - 200
    * sucessful response, usually accompanied by data
  - 202
    * info message
  - 400  
    * Somethings wrong, usually missing params
  - 404
    * either a true "missing" or an ID that doesn't exist
  - 405  
    * Method not allowed (Header shows allowed method)

### Sightings
  * [calculateDistance](#calculatedistance)
  * [delete](#delete)
  * [list](#list)
  * [view](#view)
  * [radiusSearch](#radiusSearch)
  * [save](#save)
  * [tagSearch](#tagSearch)

#### calculateDistance
  - verb: get 
  - URL: /api/sightings/calculateDistance/`{sightingId1}`/`{sightingId2}`
  - Expected json output: `success` and distance measurements OR `error`
  - Delete Status: hard
  - Response codes:
    * 200 - Success
    * 400 - No `sightingIds` provided

#### delete
  - verb: DELETE 
  - URL: /api/sightings/delete/`{sightingId}`
  - Expected output: `success` OR `error`
  - Delete Status: hard
  - Response codes:
    * 200 - Success
    * 400 - No `sightingId` provided or error deleting
    * 404 - Provided `sightingId` doesn't exist

#### list
  - verb: GET 
  - URL: /api/sightings/list
  - Expected output: list of records OR `error`
  - Response codes:
    * 200 - Success
    * 400 - No sighting records in database

#### view
  - verb: GET 
  - URL: /api/sightings/view/`{sightingId}`
  - Expected output: record retrieved OR `error`
  - Response codes:
    * 200 - Success
    * 400 - No sighting with provided `sightingId` in database

#### radiusSearch
  - verb: POST 
  - URL: /api/sightings/radiusSeach
  - Expected input: 
    `sighting` = Sighting::id
    `radius` = distance in miles
  - Expected output: `success` and `origin` and `results` OR `warn` AND `origin` OR `error`
  - Response codes:
    * 200 - Success
    * 202 - `sightingId` exists but no sightings are within provided `radius`
    * 400 - No sighting with provided `sightingId` in database

#### save
  - verb: POST 
  - URL: /api/sightings/save
  - Expected input: 
    `id` = Sighting::id (optional, only needed for update)
    `description` = text description, long text
    `latitude` = string or float
    `longitude` = string or float
    `tags` = list of tags as a string, comma separated
    `dateSighted` = string or int should work, it gets converted by DateTimeImmutable
  - Expected output: record saved OR `error` plus error messages
  - Response codes:
    * 200 - Success
    * 400 - Error saving

#### tagSearch
  - verb: POST 
  - URL: /api/sightings/tagSearch
  - Expected input: 
    `searchTags` = list of tags to search as array
    `searchParam` = AND or OR (optional, OR is assumed)
  - Expected output: `success` with a count of sightings and `results` OR `error`
  - Response codes:
    * 200 - Success
    * 404 - no records match `searchTags`



[:phalcon:]:        https://phalconphp.com/en/
[geocodeAddress]:   https://gist.github.com/chrisveness/c8b2a90bcfa91aa1e919
