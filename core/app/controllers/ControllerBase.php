<?php

use \Phalcon\Mvc\Controller;

class ControllerBase extends Controller {
    /**
     * @inheritDoc
     *
     * runs before all functions
     *
     * - add info.js
     *
     * @return void
     */
    public function initialize() 
    {
        /* info */
        $this->assets->addJs('/js/stun.js');
    }
  

    /**
     * check http method
     *
     * @return object (Phalcon Response Object | json)
     */
    protected function _checkHttpMethod($request, $response, $method = "GET") 
    {
        if ($request->getMethod() != $method) {
            $response = $this->_setResponse($response, 405, ['error' => 'Method Not Allowed'], ['Allow' => $method]);
        }

        return $response;
    }

    /**
     * set response code
     *
     * @return object (Phalcon Response Object | json)
     */
    protected function _setResponse($response, $code, $message, $headers = []) 
    {
        $response->setStatusCode($code);

        foreach ($headers as $h => $c) {
            $response->setHeader($h, $c);
        }

        if (empty($response->getContent())) {
            $response->setContent(json_encode($message));
            $response->setContentType('application/json', 'UTF-8');
        }

        return $response;
    }
}
