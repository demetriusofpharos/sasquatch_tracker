<?php

namespace Stun\Api;

class SightingsController extends \ControllerApi
{
    /**
     * @inheritDoc
     *
     * default action
     *
     * @return object (Phalcon Request Object | json)
     */
    public function indexAction() {
        $this->_checkHttpMethod($this->request, $this->response);

        if(empty($this->response->getContent())) {
            $message = 'Nothing to see here. Move along.';

            //Set the content of the response
            $this->response = $this->_setResponse($this->response, 202, ['info' => $message]);
        }

        //Return the response
        return $this->response;
    }

    /**
     * @inheritDoc
     *
     * list all sightings
     */
    public function listAction() 
    {
        $this->_checkHttpMethod($this->request, $this->response);

        if(empty($this->response->getContent())) {
            $sightings = \Sighting::find([
                'order' => 'dateSighted DESC',
            ]);
            
            //Set the content of the response
            if (empty($sightings->toArray())) {
                $sightings = ['error' => 'The system could not retrieve any sightings.'];
                $this->response = $this->_setResponse($this->response, 400, $sightings);

            } else {
                $this->response = $this->_setResponse($this->response, 200, $sightings);
            }
        }

        return $this->response;
    }

    /**
     * @inheritDoc
     *
     * view sighting
     */
    public function viewAction() 
    {
        $this->_checkHttpMethod($this->request, $this->response);

        if(empty($this->response->getContent())) {
            $sightingId = $this->dispatcher->getParams();
            $sightingId = (!empty($sightingId)) ? $sightingId[0] : '' ;

            //Set the content of the response
            if (empty($sightingId)) {
                $sighting = ['error' => 'You must provide an ID.'];
                $this->response = $this->_setResponse($this->response, 400, $sighting);

            } else {
                $sighting = \Sighting::findFirstLast('first', 'id', [
                    'conditions' => 'id=?1',
                    'bind' => [1 => $sightingId],
                ]);

                if (empty($sighting)) {
                    $sightings = ['error' => 'The system could not retrieve any sighting with that ID.'];
                    $this->response = $this->_setResponse($this->response, 404, $sighting);

                } else {
                    $this->response = $this->_setResponse($this->response, 200, $sighting);
                }
            }
        }

        return $this->response;
    }

    /**
     * @inheritDoc
     *
     * save sighting
     */
    public function saveAction() 
    {
        $this->_checkHttpMethod($this->request, $this->response, 'POST');

        if(empty($this->response->getContent())) {
            $post = $this->request->getPost();

            if (empty($post['id'])) {
                $post['id'] = uniqid();
            }

            $date = new \DateTimeImmutable($post['dateSighted']);
            $post['dateSighted'] = $date->format("Y-m-d\TH:i:s");

            $s = new \Sighting();
            $s->assign($post);
            
            if (!$s->save()) {
                $sightings = [
                    'error' => 'The system could not save this sighting.',
                    'msg' => $s->getMessages(),
                ];
                $this->response = $this->_setResponse($this->response, 400, $sightings);

            } else {
                $s->version = (!empty($s->version)) ? $s->version + 1 : 1 ;
                $s->save();
                $this->response = $this->_setResponse($this->response, 200, $s->toArray());
            }
        }

        return $this->response;
    }

    /**
     * @inheritDoc
     *
     * delete sighting
     */
    public function deleteAction() 
    {
        $this->_checkHttpMethod($this->request, $this->response, 'DELETE');

        if(empty($this->response->getContent())) {
            $sightingId = $this->dispatcher->getParams();
            $sightingId = (!empty($sightingId)) ? $sightingId[0] : '' ;

            if (empty($sightingId)) {
                $sighting = ['error' => 'You must provide an ID.'];
                $this->response = $this->_setResponse($this->response, 400, $sighting);

            } else {
                $s = \Sighting::findFirstLast('first', 'id', [
                    'conditions' => 'id=?1',
                    'bind' => [1 => $sightingId],
                ]);

                if ($s->delete()) {
                    $sighting = ['success' => 'Sighting with id ' . $sightingId . ' has been deleted.'];
                    $this->response = $this->_setResponse($this->response, 200, $sighting);

                } else {
                    $sighting = ['error' => 'The system could not find a sighting with that ID to delete.'];
                    $this->response = $this->_setResponse($this->response, 404, $sighting);
                }
            }
        }

        return $this->response;
    }

    /**
     * @inheritDoc
     *
     * given two Sighting::id entries, calculate distance
     */
    public function calculateDistanceAction() 
    {
        $this->_checkHttpMethod($this->request, $this->response);

        if(empty($this->response->getContent())) {
            $sightingId = $this->dispatcher->getParams();
            $s1         = (!empty($sightingId[0])) ? $sightingId[0] : '' ;
            $s2         = (!empty($sightingId[1])) ? $sightingId[1] : '' ;

            if (empty($s1) OR empty($s2)) {
                $sighting = ['error' => 'You must provide 2 IDs.'];
                $this->response = $this->_setResponse($this->response, 400, $sighting);

            } else {
                $s1db = \Sighting::findFirstLast('first', 'id', [
                    'conditions' => 'id=?1',
                    'bind' => [1 => $s1],
                ]); 

                $s2db = \Sighting::findFirstLast('first', 'id', [
                    'conditions' => 'id=?1',
                    'bind' => [1 => $s2],
                ]);

                $distance = \Stun\Library\Location\Distance::calculate(
                    $s1db->latitude,
                    $s1db->longitude,
                    $s2db->latitude,
                    $s2db->longitude
                );
                
                $sighting = array_merge(['success' => 'Distance calculated!'], $distance);
                $this->response = $this->_setResponse($this->response, 200, $sighting);
            }
        }

        return $this->response;
    }

    /**
     * @inheritDoc
     *
     * search by tags
     */
    public function tagSearchAction() 
    {
        $this->_checkHttpMethod($this->request, $this->response, 'POST');

        if(empty($this->response->getContent())) {
            $post = $this->request->getPost();
            $xor = (!empty($post['searchParam'])) ? $post['searchParam'] : 'OR' ;
            $x = 1;

            $query = [
                'conditions' => '',
                'bind' => [],
            ];

            foreach ($post['searchTags'] as $t) {
                if (!empty($query['conditions'])) {
                    $query['conditions'] .= ' ' . $xor . ' ';
                }

                $query['conditions'] .= 'tags LIKE ?' . $x;
                $query['bind'][$x] = "%" . $t . "%";

                $x++;
            }

            $sightings = \Sighting::find($query);

            if (empty($sightings)) {
                $sightings = ['error' => 'No sightings match the provided tags.'];
                $this->response = $this->_setResponse($this->response, 404, $sightings);

            } else {
                $results = [
                    'success' => count($sightings->toArray()) . ' sightings found.', 
                    'results' => $sightings
                ];
                $this->response = $this->_setResponse($this->response, 200, $results);
            }
        }

        return $this->response;
    }

    /**
     * @inheritDoc
     *
     * search nearby a given sighting
     */
    public function radiusSearchAction() 
    {
        $this->_checkHttpMethod($this->request, $this->response, 'POST');

        if(empty($this->response->getContent())) {
            $post = $this->request->getPost();

            if (empty($post['sighting'])) {
                $sighting = ['error' => 'You must provide an ID.'];
                $this->response = $this->_setResponse($this->response, 400, $sighting);
            
            } else {
                $sighting = \Sighting::findFirstLast('first', 'id', [
                    'conditions' => 'id=?1',
                    'bind' => [1 => $post['sighting']],
                ]);

                if (empty($sighting)) {
                    $sighting = ['error' => 'No sightings match the provided id.'];
                    $this->response = $this->_setResponse($this->response, 404, $sighting);

                } else {
                    $results['origin'] = $sighting->toArray();

                    $nearResults = \Sighting::find([
                        'conditions' => 'latitude BETWEEN :minLat: AND :maxLat: AND longitude BETWEEN :minLon: AND :maxLon:',
                        'bind' => \Stun\Library\Location\Radius::getRadius($sighting->latitude, $sighting->longitude, $post['radius']),
                    ]);

                    if (!empty($nearResults)) {
                        $nearby = [];
                        foreach ($nearResults as $r) {
                            if ($r->id != $post['sighting']) {
                                $dist = \Stun\Library\Location\Distance::calculate(
                                    $sighting->latitude,
                                    $sighting->longitude,
                                    $r->latitude,
                                    $r->longitude
                                );
                                $nearby[$r->id] = array_merge($r->toArray(), ['dist' => $dist]);
                            }
                        }

                        $results['success'] = count($nearby) . ' sightings found.'; 
                        $results['results'] = $nearby;
                        $this->response = $this->_setResponse($this->response, 200, $results);
                        
                    } else {
                        $results['warn'] = 'No sightings in given radius.';
                        $this->response = $this->_setResponse($this->response, 202, $results);
                    }
                }
            }
        }

        return $this->response;
    }
}
