<?php

class ControllerApi extends \ControllerBase {
  public $response;

  /**
   * @inheritDoc
   *
   * runs before all functions
   * additional functionality to create REST API
   * - disable GUI view
   *
   * @return void
   */
  public function initialize() {
    $query    = $this->request->getQuery();
    $cache    = (!empty($query['cache'])) ? $query['cache'] : '';

    $this->view->disable();

    //Create a response instance
    $this->response = new \Phalcon\Http\Response();
  }
}
