<?php
use Phalcon\Paginator\Adapter\NativeArray as PaginatorModel;

class StunController extends ControllerBase
{
    protected $guzzle;

    /**
     *
     */
    public function initialize() {
        parent::initialize();

        $this->guzzle = new \Stun\Library\Guzzle\GuzzleProvider($this->internal['api_url'], false);
    }

    /**
     * @inheritDoc
     */
    public function indexAction()
    {
        $this->response->redirect('/stun/listAll');
    }

    /**
     * @inheritDoc
     *
     * list all sightings
     */
    public function listAllAction() 
    {
        try {
            $sightings = $this->guzzle->get('/api/sightings/list', []);

            $page = (in_array('page', array_keys($_GET))) ? $_GET['page'] : 0;

            $paginator = new PaginatorModel([
                'data'  => json_decode($sightings->getBody(), true),
                'limit' => 100,
                'page'  => $page,
            ]);

           $sightings = $paginator->paginate();
        
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $this->flashSession->error($e->getMessage());
        }

        $this->view->setVar('sightings', (!empty($sightings)) ? $sightings : '' );
    }

    /**
     * @inheritDoc
     *
     * add sightings
     */
    public function addSightingAction() 
    {
    }

    /**
     * @inheritDoc
     *
     * edit sightings
     */
    public function editSightingAction() 
    {
        $sightingId = $this->dispatcher->getParams();
        $sightingId = (!empty($sightingId)) ? $sightingId[0] : '' ;

        if (empty($sightingId)) {
            $this->response->redirect('/stun/listAll');
        }

        try {
            $sighting = $this->guzzle->get('/api/sightings/view/' . $sightingId, []);
            $sighting = json_decode($sighting->getBody());

        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $this->flashSession->error($e->getMessage());
        }

        $this->view->setVar('sighting', (!empty($sighting)) ? $sighting : '' );
    }

    /**
     * @inheritDoc
     *
     * process post requests from add/edit 
     */
    public function processSightingAction() 
    {
        if ($this->request->isPost()) {
            $post       = $this->request->getPost();
            $sightingId = $this->dispatcher->getParams();
            $post['id'] = (!empty($sightingId)) ? $sightingId[0] : '' ;

            try {
                $sighting = $this->guzzle->post('/api/sightings/save/', ['form_params' => $post]);
                $sighting = json_decode($sighting->getBody());

            } catch (\GuzzleHttp\Exception\ClientException $e) {
                $this->flashSession->error($e->getMessage());
            }
        }

        $this->response->redirect('/stun/listAll');
    }

    /**
     * @inheritDoc
     *
     * delete sighting
     */
    public function deleteSightingAction() 
    {
        $sightingId = $this->dispatcher->getParams();
        $sightingId = (!empty($sightingId)) ? $sightingId[0] : '' ;

        if (empty($sightingId)) {
            $this->response->redirect('/stun/listAll');
        }

        try {
            $sighting = $this->guzzle->delete('/api/sightings/delete/' . $sightingId, []);
            $sighting = json_decode($sighting->getBody(), true);

            if (!empty($sighting['error'])) {
                $this->flashSession->error($sighting['error']);

            } else {
                $this->flashSession->error($sighting['success']);
            }

        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $this->flashSession->error($e->getMessage());
        }

        $this->response->redirect('/stun/listAll');
    }

    /**
     * @inheritDoc
     *
     * calculate distance
     */
    public function calculateDistanceAction() 
    {
        $sightings = \Sighting::find();
        $this->view->setVar('sightings', (!empty($sightings)) ? $sightings : '' );

        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            $this->view->setVar('post', (!empty($post)) ? $post : '' );
            $url = '/api/sightings/calculateDistance/' . $post['s1db'] . '/' . $post['s2db'];

            try {
                $distance = $this->guzzle->get($url, []);
                $distance = json_decode($distance->getBody(), true);

                if (!empty($distance['error'])) {
                    $this->flashSession->error($distance['error']);

                } else {
                    $this->flashSession->success($distance['success']);
                    $this->view->setVar('distance', (!empty($distance)) ? $distance : '' );
                }

            } catch (\GuzzleHttp\Exception\ClientException $e) {
                $this->flashSession->error($e->getMessage());
            }
        }
    }

    /**
     * @inheritDoc
     *
     * search by tags
     */
    public function tagSearchAction() 
    {
        $sightings = \Sighting::find();
        $tags = [];
        foreach ($sightings as $s) {
            if (!empty($s->tags)) {
                $sTags = explode(',', $s->tags);

                foreach ($sTags as $t) {
                    if (!in_array($t, $tags)) {
                        $tags[] = $t;
                    }
                }
            }
        }
        $this->view->setVar('tags', (!empty($tags)) ? $tags : '' );

        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            $this->view->setVar('post', (!empty($post)) ? $post : '' );

            try {
                $search = $this->guzzle->post('/api/sightings/tagSearch', ['form_params' => $post]);
                $search = json_decode($search->getBody(), true);

                if (!empty($search['error'])) {
                    $this->flashSession->error($search['error']);

                } else {
                    $this->flashSession->success($search['success']);
                    $this->view->setVar('results', $search['results'] );
                }

            } catch (\GuzzleHttp\Exception\ClientException $e) {
                $this->flashSession->error($e->getMessage());
            }
        }
    }

    

    /**
     * @inheritDoc
     *
     * get all sightings nearby a given sighting
     */
    public function nearbySightingsAction() 
    {
        $sightings = \Sighting::find();
        $this->view->setVar('sightings', (!empty($sightings)) ? $sightings : '' );

        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            $this->view->setVar('post', (!empty($post)) ? $post : '' );

            try {
                $search = $this->guzzle->post('/api/sightings/radiusSearch', ['form_params' => $post]);
                $search = json_decode($search->getBody(), true);

                if (!empty($search['error'])) {
                    $this->flashSession->error($search['error']);

                } else if (!empty($search['warn'])) {
                    $this->flashSession->warn($search['warn']);

                } else {
                    $this->flashSession->success($search['success']);
                    $this->view->setVar('radius', $post['radius'] );
                    $this->view->setVar('origin', $search['origin'] );
                    $this->view->setVar('results', $search['results'] );
                }

            } catch (\GuzzleHttp\Exception\ClientException $e) {
                $this->flashSession->error($e->getMessage());
            }
        }
    }
}
