<?php

$loader     = new Phalcon\Loader();
$dirs       = Phalcon\DI::getDefault()->get('stunconfig')->application;
$namespaces = Phalcon\DI::getDefault()->get('namespaces');

/**
 * Register a set of directories for the auto loader
 */
$registerDirs = array();
foreach ($dirs as $dir) {
  $registerDirs[] = $dir;
}
$loader->registerDirs($registerDirs)->register();

/**
 * Register namespaces
 */
$registerNamespaces = array();
foreach ($namespaces as $top => $space) {;
    foreach ($space as $s => $name) {
        if (gettype($name) == 'string') {
            $registerNamespaces[$top . "\\" . $s] = $name;
      
        } else {
            foreach ($name as $n => $dir) {
                $registerNamespaces[$top . "\\" . $s . "\\" . $n] = $dir;
            }
        }
    }
}
$loader->registerNamespaces($registerNamespaces);
