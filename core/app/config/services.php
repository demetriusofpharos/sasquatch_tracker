<?php
declare(strict_types=1);

use Phalcon\Config\Adapter\Yaml;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Escaper;
use Phalcon\Flash\Direct as Flash;
use Phalcon\Mvc\Model\Manager as ModelsManager;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\Php as PhpEngine;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Session\Adapter\Stream as SessionAdapter;
use Phalcon\Session\Manager as SessionManager;
use Phalcon\Url as UrlResolver;

/**
 * Shared configuration service
 */
if (file_exists(APP_PATH . '/config/yaml/stun.yaml')) {
    $di->setShared('stunconfig', function() {
        return new Yaml(
            APP_PATH . '/config/yaml/stun.yaml',
            [
                "!base_path" => function($value) {
                    return BASE_PATH . $value;
                },
                "!app_path" => function($value) {
                    return APP_PATH . $value;
                },
            ]
        );
    });

} else {
    die("Unable to continue due to missing config file.");
}

/**
 * set up namespaces
 */
if (file_exists(APP_PATH . '/config/yaml/namespaces.yaml')) {
    $di->setShared('namespaces', function() {
        return new Yaml(
            APP_PATH . '/config/yaml/namespaces.yaml',
            [
                "!controllers_path" => function($value) {
                    return $this->getStunconfig()->application->controllersDir . $value;
                },
                "!library_path" => function($value) {
                    return $this->getStunconfig()->application->libraryDir . $value;
                },
                "!models_path" => function($value) {
                    return $this->getStunconfig()->application->modelsDir . $value;
                },
                "!plugins_path" => function($value) {
                    return $this->getStunconfig()->application->pluginsDir . $value;
                },
                "!tasks_path" => function($value) {
                    return $this->getStunconfig()->application->tasksDir . $value;
                },
            ]
        );
    });
}

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () {
    $config = $this->getStunconfig();

    $url = new UrlResolver();
    $url->setBaseUri($config->baseUri);

    return $url;
});

/**
 * Setting up the view component
 */
$di->setShared('view', function () {
    $config = $this->getStunconfig();

    $view = new View();
    $view->setDI($this);
    $view->setViewsDir($config->application->viewsDir);

    $view->registerEngines([
        '.phtml' => PhpEngine::class
    ]);

    return $view;
});

/**
 * Database connection
 */
$di->setShared('stundb', function () {
    $config = $this->getStunconfig()->stundb;
    $class = 'Phalcon\Db\Adapter\Pdo\\' . $config->adapter;
    $params = [
        'host'     => $config->host,
        'username' => $config->username,
        'password' => $config->password,
        'dbname'   => $config->dbname,
        'charset'  => $config->charset
    ];

    if ($config->adapter == 'Postgresql') {
        unset($params['charset']);
    }

    return new $class($params);
});


/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->setShared('modelsMetadata', function () {
    return new MetaDataAdapter();
});

/**
 * set up modelsManager
 */
$di->setShared('modelsManager', function () {
    return new ModelsManager();
});

/**
 * set up environment
 */
$di->setShared('environment', function () {
    $config = $this->getStunconfig();
    return $config->environment;
});

/**
 * set up internal vars for easy retrieval
 */
$di->setShared('internal', function() {
    $config = $this->getStunconfig();
    return $config->internal;
});

/**
 * Register the session flash service with the Twitter Bootstrap classes
 */
$di->set('flashSession', function () {
    $escaper = new Escaper();
    $flash = new Flash($escaper);
    $flash->setImplicitFlush(false);
    $flash->setCssClasses([
        'error'   => 'alert alert-danger',
        'success' => 'alert alert-success',
        'notice'  => 'alert alert-info',
        'warning' => 'alert alert-warning'
    ]);

    return $flash;
});

/**
 * Start the session the first time some component request the session service
 */
$di->setShared('session', function () {
    $session = new SessionManager();
    $files = new SessionAdapter([
        'savePath' => sys_get_temp_dir(),
    ]);
    $session->setAdapter($files);
    $session->start();

    return $session;
});
