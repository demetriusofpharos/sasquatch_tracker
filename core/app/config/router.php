<?php

$router = $di->getRouter();

// Custom routes
/* <Api> */
$router->add(
    '/api/:controller/:action/:params',
    [
        'namespace'   => 'Stun\Api',
        'controller'  => 1,
        'action'      => 2,
        'params'      => 3
    ]
);
$router->add(
    '/api/:controller',
    [
        'namespace'   => 'Stun\Api',
        'controller'  => 1
    ]
);
/* </Api> */

$router->handle($_SERVER['REQUEST_URI']);
