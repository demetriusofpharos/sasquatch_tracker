<?php
namespace Stun\Library\Guzzle;

use \Stun\Library\Guzzle\GuzzleJsonException;
use \Stun\Library\Guzzle\GuzzleResponseException;

use \GuzzleHttp\Exception\RequestException;
use \GuzzleHttp\Client;

class GuzzleProvider {
  public $client;

  /**
   * @inheritDoc
   */
  public function __construct($baseURI, $verifySSL = TRUE) {
    $this->client = new Client(['base_uri' => $baseURI, 'verify' => $verifySSL]);
  }


  /**
   * @inheritDoc
   */
  public function get($uri, $headers) {
    $response = $this->client->get($uri, $headers);

    $this->checkError($response);
    return $response;
  }

  /**
   * @inheritDoc
   */
  public function getAsync($uri, $headers) {
    $response = $this->client->getAsync($uri, $headers);
    $response = $response->wait();

    return $response;
  }

  /**
   * @inheritDoc
   */
  public function post($uri, $params) {
    $response = $this->client->post($uri, $params);

    return $response;
  }

  /**
   * @inheritDoc
   */
  public function postAsync($uri, $params) {
    $response = $this->client->postAsync($uri, $params);
    $response = $response->wait();

    return $response;
  }

  /**
   * @inheritDoc
   */
  public function put($uri, $params) {
    $response = $this->client->put($uri, $params);

    return $response;
  }

  /**
   * @inheritDoc
   */
  public function delete($uri, $params) {
    $response = $this->client->delete($uri, $params);

    return $response;
  }

  /**
   * 
   */
  private function checkError($response) {
    $json = json_decode($response->getBody());

    if (json_last_error() !== JSON_ERROR_NONE) {
      throw new GuzzleJsonException();
    }

    if (isset($json->errors)) {
      foreach ($json->errors as $error) {
        throw new GuzzleResponseException($error->message, $error->code);
      }
    }

    if (isset($json->success) && ($json->success === false)) {
      throw new GuzzleResponseException("Request was unsuccessful.");
    }
  }
}
