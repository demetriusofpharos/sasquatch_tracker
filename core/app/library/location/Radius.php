<?php

//calculations based on a script found here:
//https://www.movable-type.co.uk/scripts/latlong-db.html

namespace Stun\Library\Location;

class Radius {
    /**
     * $rad = 6371; // to calculate in kilometers
     * $rad = 3959; // to calculate in miles
     */
    public static function getRadius($lat, $lon, $dist, $type = "miles") {
        $R = ($type === "miles") ? 3959 : 6371 ;

        $coordinates = [
            "minLat" => $lat - rad2deg($dist/$R),
            "maxLat" => $lat + rad2deg($dist/$R),
            "minLon" => $lon - rad2deg(asin($dist/$R) / cos(deg2rad($lat))),
            "maxLon" => $lon + rad2deg(asin($dist/$R) / cos(deg2rad($lat))),
        ];

        return $coordinates;
    }
}
