<?php
/**
 * record sightings
 */
class Sighting extends \ModelBase {
    /**
     * @ Column(type="bigint", nullable=false)
     */
    public $version;

    /**
     * @ Column(type="datetime", nullable=false)
     */
    public $dateSighted;

    /**
     * @Column (type="longtext", nullable=false)
     */
    public $description;

    /**
     * @Column (type="decimal", length=255, nullable=false)
     */
    public $latitude;

    /**
     * @Column (type="decimal", length=255, nullable=false)
     */
    public $longitude;

    /**
     * @Column (type="longtext", length=255, nullable=false)
     */
    public $tags;

    /**
     * @inheritDoc
     *
     * override the model->beforeSave function to set created
     *
     * TODO:: hmm, doesn't seem to be working. I don't think 
     * afterSave() is either
     *
     * @return boolean
     */
    public function beforeSave()
    {
        if (!$this->id) {
            $this->id = uniqid();
        }

        $this->version++;
    }
}
