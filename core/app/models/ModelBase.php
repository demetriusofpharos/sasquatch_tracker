<?php

/**
 * override Phalcon's Model to streamline the app
 */
class ModelBase extends Phalcon\Mvc\Model {
    /**
     * @Primary
     * @Identity
     * @Column (type="integer", nullable=false)
     */
    public $id;
      
    /**
     * @Column (type="integer", length=10, nullable=true)
     */
    public $created;


    /**
     * @inheritDoc
     *
     * default the connection to the appropriate DB
     *
     * @return array
     */
    public function initialize() 
    {
        $this->setConnectionService('stundb');
    }

    /**
     * @inheritDoc
     *
     * override the model->beforeSave function to set created
     *
     * @return boolean
     */
    public function beforeSave()
    {
        //if (!$this->__isset('id') && !$this->__isset('created')) {
        if (!$this->created) {
            $this->created = time();
        }
    }

    /**
     * @inheritDoc
     *
     * doesn't seem to be working so all the useable code has been removed
     */
    public function afterSave()
    {
        if (!empty($this->getMessages())) {
            $error = '';

            foreach ($this->getMessages() as $e) {
                $error .= $e . '<br />';
            }
        }
    }

    /**
     * @inheritDoc
     *
     * expand Model::findFirst to allow first or last based on col and conditions
     *
     * @param string $which ('first', 'last', or 'both')
     * @param string $col (sort by)
     * @param array $conditions (Phalcon conditions array)
     *
     * @return array
     */
    public static function findFirstLast($which, $col = 'id', $conditions = []) 
    {
        $records  = [];
        //$class    = get_class($this);

        switch ($which) {
            default:
            case 'first':
                $conditions['order'] = $col . ' ASC';
                $records[] = self::findFirst($conditions);
                break;

            case 'last':
                $conditions['order'] = $col . ' DESC';
                $records[] = self::findFirst($conditions);
                break;

            case 'both':
                $conditions['order'] = $col . ' ASC';
                $records[] = self::findFirst($conditions);

                $conditions['order'] = $col . ' DESC';
                $records[] = self::findFirst($conditions);
                break;
            }

        return (!empty($records) && count($records) == 1) ? $records[0] : $records;
    }
}
